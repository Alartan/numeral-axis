import { TestBed } from '@angular/core/testing';

import { AxisSettingsService } from './axis-settings.service';

describe('SettingsAxisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AxisSettingsService = TestBed.get(AxisSettingsService);
    expect(service).toBeTruthy();
  });
});
