import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AxisSettings } from './axis-settings';

@Injectable({
  providedIn: 'root'
})
export class AxisSettingsService {
  axisSettings: AxisSettings;

  constructor() {
    this.axisSettings = new AxisSettings();
  }

  getAxisSettings(): Observable<AxisSettings> {
    return of(this.axisSettings);
  }

  setWidth( width: number ) {
    this.axisSettings.width = width;
  }

  setHeight( height: number ) {
    this.axisSettings.height = height;
  }

}
