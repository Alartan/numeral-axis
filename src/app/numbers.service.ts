import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NumbersService {

  numbers: number[];

  constructor() {
    this.numbers = [];
  }

  getNumbers(): Observable<number[]> {
    return of(this.numbers);
  }

  addNumber(pnum: number) {
    this.numbers.push(pnum);
  }

  removeNumber(pnum: number) {
      this.numbers.splice(this.numbers.indexOf(pnum),1);
  }
}
