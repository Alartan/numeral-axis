import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointOnAxisComponent } from './point-on-axis.component';

describe('PointOnAxisComponent', () => {
  let component: PointOnAxisComponent;
  let fixture: ComponentFixture<PointOnAxisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointOnAxisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointOnAxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
