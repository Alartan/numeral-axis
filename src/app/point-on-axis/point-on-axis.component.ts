import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { throwError } from 'rxjs';

import { AxisSettingsService } from '../axis-settings.service';
import { AxisSettings } from '../axis-settings';
import { NumbersService } from '../numbers.service';
import { PointRemoveXComponent } from '../point-remove-x/point-remove-x.component';

@Component({
  selector: 'app-point-on-axis',
  templateUrl: './point-on-axis.component.html',
  styleUrls: ['./point-on-axis.component.css']
})
export class PointOnAxisComponent implements OnInit {

  buttonWidth: number = 20;

  @ViewChild('point', { static: true }) canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;

  @Input() value: number;
  pointWidth: number;
  pointPosition: number;
  public marginLeft: string;
  public buttonMarginLeft: string;
  marginTop: string;

  axisSettings: AxisSettings;
  getAxisSettings() {
    this.axisSettingsService.getAxisSettings().subscribe( settings => {
      this.axisSettings = settings;
    });
  }

  constructor(
    private axisSettingsService: AxisSettingsService,
    private numbersService: NumbersService
    ) {  }

  ngOnInit() {
    this.getAxisSettings();
    this.ctx = this.canvas.nativeElement.getContext('2d');
    try {
      this.isValueValid();
      this.pointPosition = this.initializePointPosition();
      this.pointWidth = this.initializePointWidth();
      this.marginLeft = `${this.calculateMargin()}px`;
      this.buttonMarginLeft = `${this.calculateButtonMargin()}px`;
      this.marginTop = `${this.axisSettings.height / 3 - this.axisSettings.point.height}px`;
      this.addPointToAxis();
    } catch (error) {
      console.log(error);
    }
  }

  onResize() {
    this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    this.pointPosition = this.initializePointPosition();
    this.pointWidth = this.initializePointWidth();
    this.marginLeft = `${this.calculateMargin()}px`;
    this.buttonMarginLeft = `${this.calculateButtonMargin()}px`;
    this.marginTop = `${this.axisSettings.height / 3 - this.axisSettings.point.height}px`;
    this.addPointToAxis();
  }

  isValueValid(): boolean {
    if (this.axisSettings.range.low <= this.value
      && this.value < this.axisSettings.range.high) {
        return true;
      } else { throwError(new Error('A number outside declared range.')); }
  }

  initializePointPosition() : number {
    const fullRange = this.axisSettings.range.high - this.axisSettings.range.low;
    const usableAxisWidth = (this.axisSettings.width - 3 * this.axisSettings.margin); // There needs to be space for an arrow.
    const pointPosition = usableAxisWidth * ( this.value - this.axisSettings.range.low ) / fullRange + this.axisSettings.margin;
    this.ctx.font = `${this.axisSettings.point.numberSize}px Verdana`;
    return pointPosition;
  }

  initializePointWidth(): number {
    this.ctx.font = `${this.axisSettings.point.numberSize}px Verdana`;
    this.ctx.fillStyle = 'black';
    return this.ctx.measureText(`${this.value}`).width / 2;
  }

  calculateMargin(): number {
    return this.pointPosition - this.pointWidth;
  }

  calculateButtonMargin(): number {
    return this.pointPosition - this.buttonWidth/2;
  }

  addPointToAxis() {
    // const pointPosition = usableAxisWidth * ( value - this.axisSettings.range.low ) / fullRange + this.axisSettings.margin;
    this.ctx.fillStyle = 'black';
    this.ctx.moveTo(this.pointWidth, 0);
    this.ctx.lineTo(this.pointWidth, 2 * this.axisSettings.point.height);
    this.ctx.stroke();
    this.ctx.fillText(
      `${this.value}`,
      0,
      2 * this.axisSettings.point.height + this.axisSettings.point.numberSize);
  }

  deleteNumber() {
    this.numbersService.removeNumber(this.value);
  }
}
