import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { NumbersComponent } from './numbers/numbers.component';
import { NumeralAxisComponent } from './numeral-axis/numeral-axis.component';
import { PointsListComponent } from './points-list/points-list.component';
import { PointOnAxisComponent } from './point-on-axis/point-on-axis.component';
import { PointRemoveXComponent } from './point-remove-x/point-remove-x.component';

@NgModule({
  declarations: [
    AppComponent,
    NumbersComponent,
    NumeralAxisComponent,
    PointOnAxisComponent,
    PointsListComponent,
    PointRemoveXComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
