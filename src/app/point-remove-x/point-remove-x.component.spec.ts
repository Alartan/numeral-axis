import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointRemoveXComponent } from './point-remove-x.component';

describe('PointRemoveXComponent', () => {
  let component: PointRemoveXComponent;
  let fixture: ComponentFixture<PointRemoveXComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointRemoveXComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointRemoveXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
