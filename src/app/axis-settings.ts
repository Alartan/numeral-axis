export class AxisSettings {
    width = 0;
    height = 0;
    margin = 20;
    arrowSize = 10;
    point = {
        height: 10,
        numberSize: 20,
    };
    range = {
        low: -10,
        high: 10
    };
}
