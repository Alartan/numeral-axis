import { Component, OnInit } from '@angular/core';
import { NumbersService } from '../numbers.service';
import { PointOnAxisComponent } from '../point-on-axis/point-on-axis.component';

@Component({
  selector: 'app-points-list',
  templateUrl: './points-list.component.html',
  styleUrls: ['./points-list.component.css']
})
export class PointsListComponent implements OnInit {

  listOfNumbers: number[];

  getNumbers(): void {
    this.numbersService.getNumbers().subscribe( numbers => {
      this.listOfNumbers = numbers;
     });
  }

  constructor(private numbersService: NumbersService) { }

  ngOnInit() {
    this.getNumbers();
    this.numbersService.addNumber(0);
  }

}
