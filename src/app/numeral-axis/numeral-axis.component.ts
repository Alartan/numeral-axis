import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AxisSettingsService } from '../axis-settings.service';
import { AxisSettings } from '../axis-settings';

@Component({
  selector: 'app-numeral-axis',
  templateUrl: './numeral-axis.component.html',
  styleUrls: ['./numeral-axis.component.css']
})
export class NumeralAxisComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;
  xMouse: number;
  yMouse: number;

  axisSettings: AxisSettings;
  getAxisSettings() {
    this.axisSettingsService.getAxisSettings().subscribe( settings => {
      this.axisSettings = settings;
    });
  }

  constructor(
    private axisSettingsService: AxisSettingsService
  ) {
  }

  mousePositionOnCanvas(evt: MouseEvent) {
    this.xMouse = evt.offsetX;
    this.yMouse = evt.offsetY;
  }

  onResize() {
    this.initializeCanvasSize();
    this.initializeCanvasAxis();
  }

  ngOnInit() {
    this.getAxisSettings();
    this.initializeCanvasSize();
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctx.font = `${this.axisSettings.point.numberSize}px Verdana`;
    this.ctx.fillStyle = 'black';
    this.initializeCanvasAxis();
  }

  initializeCanvasSize() {
    this.axisSettingsService.setWidth(  Math.max(window.innerWidth * 0.9, 600) );
    this.axisSettingsService.setHeight( Math.max(window.innerHeight * 0.3, 90) );
    this.canvas.nativeElement.width = this.axisSettings.width;
    this.canvas.nativeElement.height = this.axisSettings.height;
  }

  initializeCanvasAxis() {
    const axisStartX = this.axisSettings.margin;
    const axisY = this.axisSettings.height / 3;
    const axisEndX = this.axisSettings.width - this.axisSettings.margin;
    console.log(this.axisSettings);
    this.ctx.beginPath();
    this.ctx.moveTo(axisStartX, axisY);
    this.ctx.lineTo(axisEndX, axisY);
    this.ctx.stroke();
    this.drawAxisArrow({ context: this.ctx, headLen: 10, tox: axisEndX, toy: axisY });
  }

  drawAxisArrow(
    { context, headLen, tox, toy }: {
      context: CanvasRenderingContext2D;
      headLen: number;
      tox: number;
      toy: number;
    }) {
    context.moveTo(tox, toy);
    context.lineTo(tox - headLen * Math.cos(- Math.PI / 6), toy - headLen * Math.sin( - Math.PI / 6));
    context.moveTo(tox, toy);
    context.lineTo(tox - headLen * Math.cos( Math.PI / 6), toy - headLen * Math.sin( Math.PI / 6));
    context.stroke();
  }

}
