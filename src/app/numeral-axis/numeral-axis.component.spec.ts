import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumeralAxisComponent } from './numeral-axis.component';
import { PointsListComponent } from '../points-list/points-list.component';

describe('NumeralAxisComponent', () => {
  let component: NumeralAxisComponent;
  let fixture: ComponentFixture<NumeralAxisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NumeralAxisComponent,
        PointsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumeralAxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
