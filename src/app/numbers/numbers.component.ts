import { Component, OnInit } from '@angular/core';
import { NumbersService } from '../numbers.service';
// import { Liczba } from '../liczba';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent implements OnInit {

  number: number;
  listOfNumbers: number[];

  addNumber(pnumber: number) {
    this.numbersService.addNumber(pnumber);
  }

  constructor(private numbersService: NumbersService) {
    this.number = 9;
    this.listOfNumbers = [];
  }

  ngOnInit() {
  }

}
